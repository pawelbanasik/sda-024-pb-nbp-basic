package com.example.rent.sda_024_mw_nbp;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, CurrencyProvider.CurrencyAsyncTaskListener {

    private SwipeRefreshLayout swipeRefreshLayout;
    private CurrencyAdapter currencyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_dark, android.R.color.holo_green_dark, android.R.color.holo_orange_dark, android.R.color.holo_red_dark);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        currencyAdapter = new CurrencyAdapter();
        recyclerView.setAdapter(currencyAdapter);
    }

    @Override
    public void onRefresh() {
        new CurrencyProvider(this).execute("A");
    }

    @Override
    public void onDataLoaded(CurrencyTable currencyTable) {
        currencyAdapter.setData(currencyTable.getCurrencies());
        swipeRefreshLayout.setRefreshing(false);
    }
}
