package com.example.rent.sda_024_mw_nbp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-13.
 */

public class CurrencyParser {

    public CurrencyTable parseCurrencyTable(String json) throws JSONException {

        JSONArray jsonArray = new JSONArray(json);
        JSONObject jsonObject = jsonArray.getJSONObject(0);

        String table = jsonObject.getString("table");
        String number = jsonObject.getString("no");
        String effectiveDate = jsonObject.getString("effectiveDate");

        CurrencyTable currencyTable = new CurrencyTable();

        currencyTable.setTable(table);
        currencyTable.setNo(number);
        currencyTable.setEffectiveDate(effectiveDate);

        JSONArray currenciesJson = jsonObject.getJSONArray("rates");
        List<Currency> currencies = parseRatesArray(currenciesJson);
        currencyTable.setRates(currencies);


        return currencyTable;
    }

    private List<Currency> parseRatesArray(JSONArray array) throws JSONException {

        List<Currency> rates = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {

            JSONObject jsonObject = array.getJSONObject(i);

            String symbol = jsonObject.getString("code");
            String name = jsonObject.getString("currency");
            String rate = jsonObject.getString("mid");

            Currency currency = new Currency();

            currency.setCurrency(name);
            currency.setCode(symbol);
            currency.setMid(rate);

            rates.add(currency);


        }

        return rates;


    }


}
