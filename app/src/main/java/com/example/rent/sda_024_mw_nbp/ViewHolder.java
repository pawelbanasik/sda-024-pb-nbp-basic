package com.example.rent.sda_024_mw_nbp;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by RENT on 2017-05-13.
 */

public class ViewHolder extends RecyclerView.ViewHolder{

    @BindView(R.id.text_view_symbol)
    protected TextView symbolText;

    @BindView(R.id.text_view_name)
    protected TextView nameText;

    @BindView(R.id.text_view_rate)
    protected TextView rateText;


    public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setSymbolText (String symbol) {
        symbolText.setText(symbol);
    }

    public void setNameText (String name) {
        nameText.setText(name);
    }

    public void setRateText(String rate) {
        rateText.setText(rate);
    }



}
