package com.example.rent.sda_024_mw_nbp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-05-13.
 */

public class CurrencyTable {

    private String table;
    private String no;
    private String effectiveDate;
    private List<Currency> rates;

    public CurrencyTable() {
    }

    public CurrencyTable(String table, String no, String effectiveDate, List<Currency> rates) {
        this.table = table;
        this.no = no;
        this.effectiveDate = effectiveDate;
        this.rates = new ArrayList<>();
    }

    public String getTable() {
        return table;
    }

    public String getNo() {
        return no;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public List<Currency> getRates() {
        return rates;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public void setRates(List<Currency> rates) {
        this.rates = rates;
    }
}
