package com.example.rent.sda_024_mw_nbp;

/**
 * Created by RENT on 2017-05-13.
 */

public class Currency {

    private String currency;
    private String code;
    private String mid;

    public Currency() {
    }

    public Currency(String currency, String code, String mid) {
        this.currency = currency;
        this.code = code;
        this.mid = mid;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public String getMid() {
        return mid;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }
}
